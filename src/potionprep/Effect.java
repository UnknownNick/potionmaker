/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package potionprep;

/**
 *
 * @author pepik
 */
public class Effect {
    enum type {
        speed (1),
        slowness(2),
        haste(3),
        mining_fatigue(4),
        strenght(5),
        instant_health(6),
        instant_damage(7),
        jump_boost(8),
        nausea(9),
        regeneration(10),
        resistance(11),
        fire_resistance(12),
        water_breathing(13),
        invisibility(14),
        blindness(15),
        night_vision(16),
        hunger(17),
        weakness(18),
        poison(19),
        wither(20),
        health_boost(21),
        absorbtion(22),
        saturation(23),
        glowing(24),
        levitation(25),
        luck(26),
        bad_luck(27),
        slow_falling(28),
        conduit_power(29),
        dolphin_grace(30);
    
        public final int ID;
        type(int id){
            this.ID = id;
        }};
}
